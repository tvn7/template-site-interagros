export function player(video) {
  // Fonction pour mettre à jour les sources de la vidéo
  function updateVideoSources(videoElement, newSources) {
    // Retirer toutes les sources existantes
    videoElement.src(
      newSources.map((source) => ({
        type: "video/mp4",
        src: source.src,
        label: source.label,
      }))
    );
  }

  function stopVideo() {
    video.pause();
    document.querySelector(".cache").style.display = "none";
  }

  var currentlyPlaying;

  // Open video when clicking a thumbnail
  [...document.querySelectorAll(".minia")].map((minia) => {
    minia.addEventListener("click", function () {
      // Changement de source seulement quand on change de vidéo
      if (
        currentlyPlaying !=
        `${minia.getAttribute("data-type")}/${minia.getAttribute("data-video")}`
      ) {
        currentlyPlaying = `${minia.getAttribute(
          "data-type"
        )}/${minia.getAttribute("data-video")}`;

        const src = `https://data2.tvn7.fr/ia2024/${minia.getAttribute(
          "data-type"
        )}/${minia.getAttribute("data-video")}`;
        var sources;
        if (video.canPlayType('video/webm; codecs="vp9"')) {
          sources = [
            { src: `${src}_1080.webm`, label: "1080P" },
            { src: `${src}_720.webm`, label: "720P" },
            { src: `${src}_360.webm`, label: "360P" },
          ];
        } else if (video.canPlayType('video/mp4; codecs="hev1"')) {
          sources = [{ src: `${src}_hevc.mp4`, label: "1080P" }];
        } else {
          sources = [{ src: `${src}_h264.mp4`, label: "1080P" }];
        }

        updateVideoSources(video, sources);
      }

      document.querySelector(".cache").style.display = "block";
      video.play();
    });
  });

  // Close video on click outside of video
  document.querySelector(".cache").addEventListener("click", function (event) {
    if (!event.target.closest("#video")) {
      stopVideo();
    }
  });

  // Close player on pressing esc
  document.addEventListener("keydown", function (event) {
    if (event.key === "Escape") {
      stopVideo();
    }
  });
}

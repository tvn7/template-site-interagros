import { player } from "./player.js";

const video = videojs("video", {}, function () {
  var player = this;
  player.controlBar.addChild("QualitySelector");
});

document.addEventListener("DOMContentLoaded", () => {
  const jsonUrl = "/data.json";

  fetch(jsonUrl)
    .then((response) => response.json())
    .then((data) => {
      const container = document.getElementById("container");

      data.videos.forEach((videoCategory) => {
        const section = document.createElement("section");

        const h1 = document.createElement("h1");
        h1.textContent = videoCategory.name;
        section.appendChild(h1);

        const sliderDiv = document.createElement("div");
        sliderDiv.classList.add("slider");

        videoCategory.data.forEach((video) => {
          const videoDiv = document.createElement("div");
          videoDiv.classList.add("minia");
          videoDiv.setAttribute("data-video", video.name);
          videoDiv.setAttribute("data-type", videoCategory.folderName);
          videoDiv.style.backgroundImage = `url('/assets/images/${video.thumbnail}')`;
          sliderDiv.appendChild(videoDiv);
        });

        section.appendChild(sliderDiv);

        if (videoCategory.p) {
          const paragraph = document.createElement("p");
          paragraph.innerText = videoCategory.p;
          section.appendChild(paragraph);
        }

        container.appendChild(section);
      });
      player(video); // appel du script pour gérer les vidéos une fois que toutes les miniatures sont chargées
    })
    .catch((error) => console.error("Error fetching JSON data:", error));
});
